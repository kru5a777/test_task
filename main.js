var products = [];

function initApp() {
    products.forEach(function(object) {
    var template = $('#template').html();
    var attributes = Object.keys(object);
        
    attributes.forEach(function(attr) {
        var value = object[attr];

            if (attr == 'code') {
                value = parseInt(value);
            }

            if (attr == 'primaryImageUrl') {
                var splitStringUrl = value.split(".");
                splitStringUrl[splitStringUrl.length - 2] += '_220x220_1';
                value = splitStringUrl.join(".");
            }

            template = template.replace('{'+ attr +'}', value);
        });

        $('#product-list').append(template);
    });

    $('.product__area').on('click', '.stepper-arrow', function() {
        var currentValue = parseInt($(this).siblings('input').val());
        if ($(this).hasClass('up')) {
            $(this).siblings('input').val(currentValue+1);
        } else if (currentValue > 1) {
            $(this).siblings('input').val(currentValue-1);
        } 
    });

    $('.product__area').on('click', '.unit--wrapper > div', function() {
        var currentProductElement = $(this).closest('.product_horizontal');
        var currentId = currentProductElement.find('.btn_cart').attr('data-product-id');
        var forWhat = [];

        if ($(this).hasClass('for-unit')) {
            forWhat = ['priceRetail','priceGold']
        } else {
            forWhat = ['priceRetailAlt','priceGoldAlt']
        }

        products.forEach(function(product) {
            if (product['productId'] == currentId) {
                forWhat.forEach(function(attr) {
                    if (attr.indexOf('Gold')+1) {
                        $(currentProductElement).find('.goldPrice').text(product[attr].toFixed(2));
                    } else {
                        $(currentProductElement).find('.retailPrice').text(product[attr].toFixed(2));
                    }
                });
            }
        });
        $(this).siblings('div').removeClass('unit--active');
        $(this).addClass('unit--active');
    });
}

$(document).ready(function(event) {
    $.getJSON('products.json', function(data) {
        products = data;
        initApp();
    });
});