var gulp = require('gulp'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass');

 
/*
gulp.task('default', function () {
    return watch('main.js', { ignoreInitial: false })
        .pipe(gulp.dest('build/js'));
});
 
gulp.task('callback', function () {
    return watch('main.js', function () {
        gulp.src('main.js')
            .pipe(gulp.dest('build/js'));
    });
});
*/

gulp.task('sass', function() {
	gulp.src('*.scss')
		.pipe(sass())
		.pipe(gulp.dest('build/css'));
});